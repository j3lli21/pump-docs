Running the daemon
==================

The recommended way to run the pump.io daemon is to use the systemd
service file that's shipped with the package. This service file
automatically sets many important options, such as the ``NODE_ENV``
environment variable (which impacts performance) and several
security-related systemd settings. See `"Using the upstream pump.io
systemd unit" <../administration/upstream-systemd-unit.html>`_ for
detailed instructions, and note that you can override parts of it if
you need using `drop-in files
<https://wiki.archlinux.org/index.php/Systemd#Drop-in_files>`_.

If you can't use the systemd unit, you need to arrange to run either
``./bin/pump`` in your git clone (if you installed from souce) or
``pump`` (if you installed from npm). You'll probably get a more
reliable experience if you use your operating system's built-in
``init`` service manager, or you could use `forever
<https://npmjs.org/package/forever>`_ on npm to keep the daemon
running.
