Clients and Services
====================

These are some client applications and services that support Pump.io.

See also: `Libraries <https://github.com/pump-io/pump.io/wiki/Libraries>`_


Web
---

* `OFirehose <https://ofirehose.com>`_ - The pump network firehose.
* `pump2rss.com <https://pump2rss.com>`_ - Generates an RSS (Atom) feed of the activity stream.
* `pumpiostatus.website <http://pumpiostatus.website>`_ - Check the status and uptime of all registered Pump servers.
* `Granada <https://gitlab.com/sotitrox/granada>`_- Website, bookmarklet and button for publishing content from other places in Pump.io ("Share in Pump.io" thingy).
* `PPump <ttps://gitlab.com/sotitrox/ppump>`_ - Ppump: RSS Feed of the Firehose and public user directory.
* `PumpBridge <https://wald.intevation.org/projects/pumpbridge/>`_ - Connects Pump.io to facebook and googleplus.



Desktop
-------

* `Dianara <https://jancoding.wordpress.com/dianara>`_ - A Qt desktop app.
    * `MSwindows builds <https://www.luisgf.es/pump/>`_ of Dianara. Website in Spanish, for now.
* `Pumpa <http://saz.im/software/pumpa.html>`_ - Another Qt client under development.
    * `How to <https://github.com/e14n/pump.io/wiki/HowTo-for-building-Pumpa-on-OS-X>`_ for building Pumpa on OS X.
* `Choqok <http://choqok.gnufolks.org/>`_ - KDE micro-blogging client.

* `spigot <https://pypi.python.org/pypi/spigot/>`_ - Console client for rate-limited (RSS) feed aggregation. Implemented in Python via PyPump.
* `PumpTweet <https://github.com/dper/PumpTweet>`_ - Find notes from your Pump account, shorten them, make a URL to the original note, and post the short version as a tweet on Twitter. It can also be used for GNU Social (StatusNet).
* `PumpMigrate <https://github.com/kabniel/pumpmigrate>`_ - Move or sync contacts between Pump.io accounts.
* `p <https://github.com/xray7224/p>`_ - A Pump.io version of the command line utility 't'.
* `NavierStokes <http://polari.us/dokuwiki/doku.php?id=navierstokes>`_ - Allows you to bridge between social network accounts. ALPHA release.
* `Pumpio-el <http://www.emacswiki.org/emacs/?action=browse;oldid=Pumpio-el;id=PumpioEl>`_ - Pump.io client for Emacs.
* `GPump <https://launchpad.net/gpump>`_ - A GTK+ Pump.io client in the concept stages.
* `Manivela <https://gitlab.com/sotitrox/manivela>`_ - Command line client written in PHP. Documentation in Spanish.



Android
-------

* `Impeller <http://impeller.e43.eu/>`_ - ICS (4.0) or above - `Google Play <https://play.google.com/store/apps/details?id=eu.e43.impeller>`_ / `Download APK <https://dl.dropboxusercontent.com/u/64151759/Impeller.apk>`_
* `Puma <https://gitorious.org/puma-droid>`_ - `Download APK <http://macno.org/puma/>`_
* `PumpFM <https://gitorious.org/pumpfm>`_ - A simple app that scrobbles the music listened on your Android phone to a Pump.io instance (`Link to binary .apk <https://static.jpope.org/files/Pumpfm.apk>`_)
* `AndStatus <https://github.com/andstatus/andstatus/wiki>`_ - Multiple Pump.io, GNU Social and Twitter accounts. Can work offline.



iOS
---

* `Social Monkeys <http://www.roguemonkey.in/>`_ - An intuitive  iOS client to manage your Pump.io social activity stream.
